
## 0.2.5 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/meraki-sd-wan-management!22

---

## 0.2.4 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/meraki-sd-wan-management!21

---

## 0.2.3 [01-18-2022]

* 21.2 certification

See merge request itentialopensource/pre-built-automations/meraki-sd-wan-management!20

---

## 0.2.2 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/meraki-sd-wan-management!19

---

## 0.2.1 [03-18-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/meraki-sd-wan-management!18

---

## 0.2.0 [06-18-2020]

* [minor/LB-404] Add './' to img path

See merge request itentialopensource/pre-built-automations/meraki-sd-wan-management!13

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Meraki-SD-WAN-Management!12

---

## 0.0.11 [04-17-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!11

---

## 0.0.10 [04-17-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!11

---

## 0.0.9 [04-15-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!11

---

## 0.0.8 [03-30-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!7

---

## 0.0.7 [03-10-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!7

---

## 0.0.6 [03-06-2020]

* Add artifacts keyword

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!6

---

## 0.0.5 [02-26-2020]

* Add artifacts keyword

See merge request itentialopensource/pre-built-automations/sd-wan-management-meraki!6

---

## 0.0.4 [02-25-2020]

* Update dependencies for 2019.3

See merge request itentialopensource/pre-built-automations/staging/sd-wan-management-meraki!4

---

## 0.0.3 [02-25-2020]

* Update dependencies for 2019.3

See merge request itentialopensource/pre-built-automations/staging/sd-wan-management-meraki!4

---

## 0.0.2 [02-25-2020]

* Update dependencies for 2019.3

See merge request itentialopensource/pre-built-automations/staging/sd-wan-management-meraki!4

---\n\n
