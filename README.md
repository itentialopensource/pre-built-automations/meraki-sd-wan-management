# SD-WAN Branch Management

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Features](#features)
* [Requirements](#requirements)
* [Known Limitations](#known-limitations)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Components](#components)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the pre-built and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
The SD-WAN Branch Management pre-built consists of a series of small modular workflows that creates a 'network' for a given organization and claims one or many access points in that network. A demonstration workflow orchestrates the process, and also indicates how to integrate with a ticketing system, such as ServiceNow.

<table><tr><td>
  <img src="./images/overview.png" alt="workflow" width="800px">
</td></tr></table>

## Features
* Operation Modes: Zero Touch and Normal mode
* Onboard devices into your SDWAN environment
* Pre/Post checks for your deployment
* Notifications throughout the onboarding process
<!-- Unordered list highlighting the most exciting features of the pre-built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->

## Requirements

In order to use this pre-built, users will have to satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2021.2`
* Meraki Adapter
  * `^0.6.0`

Please refer to the documentation for the [Meraki Adapter](https://gitlab.com/itentialopensource/adapters/sd-wan/adapter-meraki) in order to install and configure the adapter.

## Known Limitations

This pre-built currently only supports branch management for Meraki controllers and access points. A future update may include support for Viptella, VeloCloud and SilverPeak devices.

## How to Install

Please ensure that you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 

The pre-built can be installed from within App - Admin Essentials. Simply search for the name of your desired pre-built and click the install button as shown below:

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->


<!-- OPTIONAL - Explain if external components are required outside of IAP. Ex.: The Ansible roles required for this pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

The pre-built includes a demonstration for Meraki controllers which will create a network branch within an organization and claim one or more devices to that network. In order to run the demonstration, run the `Meraki Network Deployment` item from Operations Manager:

<table><tr><td>
  <img src="./images/ac.png" alt="install" width="400px">
</td></tr></table>

You will then be presented a form with some necessary information to provide in order to run the demo:

<table><tr><td>
  <img src="./images/json-form.png" alt="install" width="600px">
</td></tr></table>

* **Organization Name**: Provide the name of the organization available to the Meraki controller within which you would like to add the network branch
* **Network Name**: Provide a name for the network branch which will be created
* **Email**: Provide a valid email address in order to receive notifactions throughout the demonstration, including alerts from the device
* **Devices**: Provide a list of serial numbers for the devices you would like to claim to the network. You may add another device by pressing the `+` button in the bottom right hand corner

This demonstration may be run in verbose mode, which will display alerts in manual tasks. Additionally, the demonstration will stop at various breakpoints in this mode which require the user to work a manual task in order to progress. These breakpoints allow for an opportunity to examine the Meraki Dashboard and gain an understanding of what the Meraki workflows are doing under the hood.

<table><tr><td>
  <img src="./images/verbosemode.png" alt="install" width="400px">
</td></tr></table>

When you are ready, press the `Run` button to begin the automation. When the demonstration has completed, the workflow will rollback any changes made, such as removing the network branch and any claimed devices.

## Components

This pre-built contains a set of modular workflows which are designed to be used together to create network branches and claim devices to those branches. Please see the demonstration workflow for an example of how to use these workflows, as well as how to integrate with notification and ticketing systems. The information below describes how to use each workflow, and what variables must be provided as input.

### Create Network

<table><tr><td>
  <img src="./images/createnetwork.png" alt="install" width="800px">
</td></tr></table>

This workflow creates a ‘network’ within a Meraki management platform. If the network already exists, the ID of that existing network is returned.

#### Input Variables

The following input variables are required:

* `input`: The input variable is a JSON object which will be used as the payload to the [Create Organization Network](https://developer.cisco.com/meraki/api/#/rest/models/structures/create-organization-network) API call. Please reference the API documentation for full details on how this payload should be formed.
* `organizationName`: The name of the organization within which to create the network branch.

The following JSON object provides an example for the request payload:

```json
{
  "organization": "Acme",
  "name": "demo network",
  "type": "wireless",
  "tags": " iap demo ",
  "timeZone": "America/New_York",
  "disableMyMerakiCom": false,
  "disableRemoteStatusPage": true
}
```

In order to create this object, either the `makeData` task or the `merge` task may be used in your workflow before using the `childJob` task to call the **Create Network** workflow.

#### Validation

This workflow ensures the following:

* Check if the organization exists within the Meraki management platform
* Check if the Network already exists

#### Error Handling

Each error thrown is assigned a code:

* `401` – Organization not found
* `402` – An error occurred when attempting to create the network. The response from the API call is returned within the ‘errorStack’ parameter of the response. See details below.

#### Response

If invoked as a child job, the outgoing data payload includes all job variables defined within the workflow. The specific result of the workflow is defined within the `output` variable.

If successful, `output` will return:

* `code`: 200
* `OrganisationId`: This is the Id of the inputted organization. This is particularly useful as it is required for other API calls to Meraki
* `NetworkId`: The id of the network
* `network`: This flag will tell you whether the workflow created a new network or found an existing one in the given organization

##### Example Success Output

```json
"output": {
  "code": 200,
  "status": "success",
  "description": {
    "code": 200,
    "result": "success",
    "OrganisationId": "12345",
    "NetworkId": "N_647955396388013760",
    "network": "created"
  }
}
```

##### Example Error Output

```json
"output": {
  "code": 401,
  "status": "error",
  "description": "The organisation does not exist in the Meraki Network.",
  "errorStack": null,
  "errorRecommendation": "Check the value of the inputted organization exists within the Meraki platform."
}
```

### Claim All Devices

<table><tr><td>
  <img src="./images/claimall.png" alt="install" width="800px">
</td></tr></table>

This workflow accepts an array of devices in order for them to be claimed into a given pre-existing Network. For each element in `networkDevice` array, the sub-workflow [Claim Device](#claim-device) is invoked.

#### Input Variables

The following input variables are required:

* `networkDevice`: An array of serial numbers for the devices to claim. This array should be in the following format:	

  ```json
  [
    {
      "serial": "1234-1234-1234"
    },
    {

      "serial": "9876-9876-9876"
    }
  ]
  ```

* `organizationId`:	The ID for the organization, e.g. **12345**
* `NetworkId`: The ID for the network, e.g. **N_647955396388013760**

#### Validation

Upon completion, the status of the child [Claim Device](#claim-device) job is evaluated and depending on success or failure the job variable arrays are updated with the result.

If the `output.status` of the child job does not equal `success` then the child job is deemed to have failed.

On success, an element is added to the resultArray. The element details are:

* `deviceId`: serial number of the claimed device
* `response`: claimed
* `status`: success

On failure, an element is added to the resultArray and the errorArray. The element details for a failure are:

* `deviceId`: serial number of the claimed device
* `networkId`: the ID for the network
* `organizationId`: the ID for the organization
* `response`: the `output` variable from the child job
* `status`: error

#### Error handling

Failures/errors from the child [Claim Device](#claim-device) job are captured and added to the error array.

#### Response

If the count of elements in the `errorArray` is greater than 0, then an error response is returned.

##### Example Success Output

```json
{
  "code": 200,
  "status": "success",
  "description": "All devices successfully claimed"
}
```

##### Example Error Output

In the example below, the overall status of the job is an error, and the errorStack has the result of each job.

```json
{
  "code": 401,
  "status": "error",
  "description": "An error occurred claiming one (or more) devices.",
  "errorStack": [
    {
      "deviceId": {
        "serial": "1234-1234-1234"
      },
      "response": "claimed",
      "status": "success"
    },
    {
      "deviceId": {
        "serial": "1234-1234-TEST"
      },
      "networkId": "N_647955396388014332",
      "organizationId": "12345",
      "response": {
        "code": 402,
        "status": "error",
        "description": "An error occured whilst create the network in the Meraki Controller",
        "errorStack": {
          "icode": "AD.500",
          "IAPerror": {
            "origin": "Meraki-connectorRest-handleEndResponse",
            "displayString": "Error 400 received on request",
            "recommendation": "Verify the request is accurate via debug logs and postman",
            "code": 400,
            "raw_response": {
              "status": "success",
              "code": 400,
              "response": "{\"errors\":[\"Device with serial 1234-1234-TEST not found\"]}"
            }
          }
        },
        "errorRecommendation": "The device could not be claimed. Please check the errorStack for details. It's possible the device doesn't exist for it to be claimed."
      },
      "status": "error"
    }
  ]
}
```

### Claim Device

<table><tr><td>
  <img src="./images/claimdevice.png" alt="install" width="800px">
</td></tr></table>

This workflow claims a device into an organization's network. It does so by calling the [Meraki API](https://developer.cisco.com/meraki/api/#/rest/api-endpoints/devices/claim-network-devices).

#### Input Variables

The following input variables are required:

* `networkDevice`: the serial number should be provided as a JSON object in the following format:

  ```json
  {
    "serial": "1234-1234-1234"
  }
  ```

* `organizationId`: the ID for the organization, e.g. **812345**
* `NetworkId`: the ID for the network, e.g. **N_647955396388013760**

#### Validation

A check is performed to ensure the device that’s being claimed has not already been assigned to any network within the organization. If it has, then the details of the network are fetched and returned, along with the status `error`.

#### Error Handling

Each error thrown is assigned a code:

* `401`: The device that’s being claimed is already assigned to a network within the organization

#### Response

If the device has been successfully claimed, the following parameters are returned:

* `Code`: 200
* `Status`: success

Any other response can be considered a failure.

##### Example Success Output

```json
{
  "code": 200,
  "status": "success",
  "description": "All is Well"
}
```

##### Example Error Output

```json
{
  "code": 401,
  "status": "error",
  "description": "The device with Serial Number 1234-1234-1234' is already assigned to an Organizations network.",
  "errorStack": {
    "id": "N_647955396388014332",
    "organizationId": "12345",
    "type": "wireless",
    "productTypes": [
      "wireless"
    ],
    "name": "demo network",
    "timeZone": "America/New_York",
    "tags": " demo iap ",
    "disableMyMerakiCom": false,
    "disableRemoteStatusPage": true,
    "enrollmentString": null
  },
  "errorRecommendation": "Check the serial number of the device. If you believe it to be correct, try moving the device from its existing network to the new one."
}
```
## Additional Information
Please use your Itential Customer Success account if you need support when using this pre-built.
